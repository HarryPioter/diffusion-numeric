CC=clang++
CFLAGS=-Wall -Wextra -O2

sim: Source.cpp
	$(CC) $(CFLAGS) Source.cpp -o $@
