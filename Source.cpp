#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <sstream>
#include <tuple>
#include <vector>

using std::array;
using std::begin;
using std::cerr;
using std::copy;
using std::end;
using std::exp;
using std::fill;
using std::function;
using std::ofstream;
using std::pair;
using std::string;
using std::stringstream;
using std::vector;

struct simulation_params {
  float T;
  float T_change_rate;
  int iterations;
};

constexpr int arr_len = 100, series_n = 5;
constexpr float R = 8.3144f, dt = 0.001f, dx = 0.1f, c_steel = 0.1f,
                c_ferrite = 0.02f;
float phase_transition_concetration(float T);
float diffusion_coefficient(float Q, float D0, float T);
float calculate_concentration(float param, float prev_c, float prev_x,
                              float next_x);
float integrate(int ksi, const array<float, arr_len> &c);
void save_austenite_fraction_series(const vector<pair<int, float>> &fractions,
                                    ofstream &file);
template <typename Container>
void save_to_csvv(
    const Container &con, simulation_params params, string filename_prefix,
    function<void(const decltype(con) &, ofstream &)> collection_handler) {
  stringstream filename;
  filename << filename_prefix << params.T << "K_" << params.T_change_rate
           << "dT_" << params.iterations << "iter.csv";
  ofstream file(filename.str());
  if (file.good())
    collection_handler(con, file);
  else
    cerr << "Couldn't create file\n";
}

int main(int argc, char const *argv[]) {
  assert(argc > 2);
  auto time_steps_n = atoi(argv[1]);
  float T = atof(argv[2]);
  assert(time_steps_n > 0);
  assert(T >= 0.0f);
  float dT = argc == 3 ? 0.f : (float)atof(argv[3]);

  using std::cbegin;
  using std::cend;
  vector<array<float, arr_len>> series;
  series.reserve(series_n);
  array<array<float, arr_len>, 2> c{0};
  auto ksi = 25;
  {
    // Initialization of fixed buffers
    auto &frst_arr = c.front();

    auto start = begin(frst_arr);
    auto ferrite_boundary = begin(frst_arr) + ksi;
    auto end = frst_arr.end();
    auto c_ga = phase_transition_concetration(T);

    fill(start, ferrite_boundary, c_ferrite);
    *ferrite_boundary++ = c_ga;
    fill(ferrite_boundary, end, c_steel);
    copy(start, end, begin(c.back()));
  }
  constexpr float Q = 1.4E5f, d0 = 4.1E-5f;
  auto stride = time_steps_n / series_n - 1;
  auto dT_per_step = dT * dt;
  auto simul_params = simulation_params{T, dT, time_steps_n};
  vector<pair<int, float>> austenite_fractions;
  {
    const auto initial_integral = integrate(ksi, c.front());
    auto integral_val = initial_integral;
    for (auto i = 0; i < time_steps_n; ++i) {
      float D = diffusion_coefficient(Q, d0, T);
      auto param = (D * dt) / (dx * dx);

      c[1][ksi] = phase_transition_concetration(T);
      for (auto j = ksi + 1; j < arr_len - 1; ++j)
        c[1][j] =
            calculate_concentration(param, c[0][j], c[0][j - 1], c[0][j + 1]);
      c[1][arr_len - 1] = calculate_concentration(
          param, c[0][arr_len - 1], c[0][arr_len - 2], c[0][arr_len - 1]);

      auto integral = integrate(ksi, c.front());
      if (ksi < arr_len - 1 &&
          integral - integral_val >= (c_steel - c_ferrite) * dx) {
        c[1][ksi++] = c_ferrite;
        c[1][ksi] = phase_transition_concetration(T);
        integral_val = integral;
        austenite_fractions.emplace_back(i, 1.0f - static_cast<float>(ksi) /
                                                       arr_len);
      }
      copy(cbegin(c[1]), cend(c[1]), begin(c[0]));

      T += dT_per_step;
      if (i % stride == 0)
        series.push_back(c[0]);
    }
  }
  save_to_csvv(series, simul_params, "log_",
               [&](const vector<array<float, arr_len>> &sr, ofstream &file) {
                 const auto iter_stride = simul_params.iterations / series_n;
                 file << ';';
                 for (auto i = 0; i < arr_len; ++i)
                   file << i << ';';
                 file << '\n';

                 for (std::size_t i = 0; i < sr.size(); ++i) {
                   auto time = iter_stride * i * dt;
                   file << time << "s;";
                   for (const auto &c : sr[i])
                     file << c << ';';
                   file << '\n';
                 }
               });
  save_to_csvv(austenite_fractions, simul_params, "fraction_",
               save_austenite_fraction_series);
  return 0;
}

float phase_transition_concetration(float T) {
  if (T > 1185.0f)
    return 0.0f;
  else
    return 2.30965E-5f * T * T - 5.46279E-2f * T + 32.3015f;
}

float diffusion_coefficient(float Q, float D0, float T) {
  return D0 * exp(-Q / (R * T)) * 1E10f;
}

float calculate_concentration(float param, float prev_c, float prev_x,
                              float next_x) {
  return (1.f - 2.f * param) * prev_c + param * (prev_x + next_x);
}

float integrate(int ksi, const array<float, arr_len> &c) {
  // Trapeze int
  auto acc = 0.0f;
  for (auto i = ksi; i < c.size() - 1; ++i) {
    acc += c[i] + c[i + 1];
  }
  return acc * dx * 0.5f;
}

void save_austenite_fraction_series(const vector<pair<int, float>> &fractions,
                                    ofstream &file) {
  for (const auto &pair : fractions)
    file << pair.first << ';' << pair.second << ";\n";
}
